
package model;


public class Produto {
    
    private String tipo;
    private String id;
    private String preco;
    private String quantidade;
    private String quantidadeMin;

    public Produto() {
    }

    public Produto(String tipo, String id, String preco, String quantidade, String quantidadeMin) {
        this.tipo = tipo;
        this.id = id;
        this.preco = preco;
        this.quantidade = quantidade;
        this.quantidadeMin = quantidadeMin;
    }

    
    
    
    public String toString() {
        return "Id: "+ getId() + "\n" +"Tipo: " + getTipo() + "\n" + "Preço: " +  getPreco() + "\n"+ "Quantidade: " +  getQuantidade() + "\n" +"Quantidade Mínima: "+  getQuantidadeMin() +"\n"; 
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

    public String getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(String quantidade) {
        this.quantidade = quantidade;
    }

    public String getQuantidadeMin() {
        return quantidadeMin;
    }

    public void setQuantidadeMin(String quantidadeMin) {
        this.quantidadeMin = quantidadeMin;
    }
    
    
    
}
