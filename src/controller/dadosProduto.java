
package controller;

import java.util.ArrayList;

import model.Produto;

public class dadosProduto {
    
    
    private ArrayList<Produto> listaProdutos;
    
    public dadosProduto() {
        this.listaProdutos = new ArrayList<Produto>();
    }
    
    public ArrayList<Produto> getListaProduto() {
        return listaProdutos;
    }
    
    
    public void cadastraProduto (Produto produto) {
        listaProdutos.add(produto);
    }
 
    public void removerProduto(Produto produto) {
        listaProdutos.remove(produto);
    }
    
    public Produto pesquisar(String nome) {
        for (Produto p: listaProdutos) {
            if (p.getTipo().equalsIgnoreCase(nome)) return p;
        }
        return null;
    }
            
    
    
    
}
