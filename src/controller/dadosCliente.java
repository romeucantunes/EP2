
package controller;

import java.util.ArrayList;

import model.Cliente;


public class dadosCliente {
    
    
    private final ArrayList<Cliente> listaClientes;
   
    
    public dadosCliente() {
        this.listaClientes = new ArrayList<>();
    }
    
    public ArrayList<Cliente> getListaCliente() {
        return listaClientes;
    } 
    
    public void cadastraCliente(Cliente cliente) {
        listaClientes.add(cliente);        
    }
    
    public void removerCliente(Cliente cliente) {
        listaClientes.remove(cliente);
    }
    
    public Cliente pesquisar(String nome) {
        for (Cliente c: listaClientes) {
            if (c.getNome().equalsIgnoreCase(nome)) return c;
        }
        return null;
    }
    
    
  
    
}
