
package view;

import controller.ValidaEntrada;
import controller.ValidaEntradaLetra;
import controller.dadosProduto;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.Cliente;
import model.Produto;


public class TelaPrincipal extends javax.swing.JFrame {

        dadosProduto dadosProduto = new dadosProduto();

    public TelaPrincipal() {
        initComponents();
        jTextFieldNome.setDocument(new ValidaEntradaLetra());
        jTextFieldPreco.setDocument(new ValidaEntrada());
        jTextFieldQuantidade.setDocument(new ValidaEntrada());
        jTextFieldQuantidadeMin.setDocument(new ValidaEntrada());
        jTextFieldId.setDocument(new ValidaEntrada());
        
        
    }

    private void carregarListaProdutos() {
        ArrayList<Produto> listaProdutos = dadosProduto.getListaProduto();
        DefaultTableModel model = (DefaultTableModel) jTableProduto.getModel();
        model.setRowCount(0);
        for (Produto p : listaProdutos) {
            model.addRow(new String[]{p.getId(),p.getTipo(),p.getPreco(),p.getQuantidade(),p.getQuantidadeMin()});
            
        }
        jTableProduto.setModel(model);
    }
    private void limpaLista() {
   
    DefaultTableModel model = (DefaultTableModel) jTableCompraProdutos.getModel();
    while (jTableCompraProdutos.getModel().getRowCount() > 0)
    model.removeRow(0);
    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jButtonVoltar = new javax.swing.JButton();
        jLabelNome = new javax.swing.JLabel();
        jLabelId = new javax.swing.JLabel();
        jLabelPreco = new javax.swing.JLabel();
        jLabelQuantidade = new javax.swing.JLabel();
        jLabelQuantidadeMIn = new javax.swing.JLabel();
        jTextFieldNome = new javax.swing.JTextField();
        jTextFieldId = new javax.swing.JTextField();
        jTextFieldPreco = new javax.swing.JTextField();
        jTextFieldQuantidade = new javax.swing.JTextField();
        jTextFieldQuantidadeMin = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jInternalFrame1 = new javax.swing.JInternalFrame();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableCompraProdutos = new javax.swing.JTable();
        jButtonPagar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jButtonSalvar = new javax.swing.JButton();
        jButtonExcluir = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableProduto = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setSize(new java.awt.Dimension(833, 546));
        getContentPane().setLayout(null);

        jPanel1.setLayout(null);

        jButtonVoltar.setText("Voltar");
        jButtonVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVoltarActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonVoltar);
        jButtonVoltar.setBounds(600, 290, 110, 24);

        jLabelNome.setText("Nome :");
        jPanel1.add(jLabelNome);
        jLabelNome.setBounds(20, 60, 150, 30);

        jLabelId.setText("Id :");
        jPanel1.add(jLabelId);
        jLabelId.setBounds(20, 100, 150, 30);

        jLabelPreco.setText("Preço :");
        jPanel1.add(jLabelPreco);
        jLabelPreco.setBounds(20, 140, 150, 30);

        jLabelQuantidade.setText("Quantidade :");
        jPanel1.add(jLabelQuantidade);
        jLabelQuantidade.setBounds(20, 180, 150, 30);

        jLabelQuantidadeMIn.setText("Quantidade Min :");
        jPanel1.add(jLabelQuantidadeMIn);
        jLabelQuantidadeMIn.setBounds(20, 220, 150, 30);
        jPanel1.add(jTextFieldNome);
        jTextFieldNome.setBounds(70, 60, 300, 30);
        jPanel1.add(jTextFieldId);
        jTextFieldId.setBounds(60, 100, 310, 30);
        jPanel1.add(jTextFieldPreco);
        jTextFieldPreco.setBounds(70, 140, 300, 30);
        jPanel1.add(jTextFieldQuantidade);
        jTextFieldQuantidade.setBounds(110, 180, 320, 30);
        jPanel1.add(jTextFieldQuantidadeMin);
        jTextFieldQuantidadeMin.setBounds(130, 220, 300, 30);

        jButton1.setText("Adicionar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(460, 40, 120, 24);

        jButton2.setText("Remover");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(460, 80, 120, 24);

        jInternalFrame1.setVisible(true);

        jTableCompraProdutos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Produto", "Preço"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTableCompraProdutos);

        jInternalFrame1.getContentPane().add(jScrollPane2, java.awt.BorderLayout.CENTER);

        jPanel1.add(jInternalFrame1);
        jInternalFrame1.setBounds(630, 20, 210, 260);

        jButtonPagar.setText("Pagar");
        jButtonPagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPagarActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonPagar);
        jButtonPagar.setBounds(730, 290, 120, 24);

        jLabel2.setText("Lista de Compras");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(630, 0, 210, 14);

        jLabel3.setText("Adicionar ao Estoque !");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(30, 20, 190, 14);

        jLabel4.setText("Botões da LIsta de Compras");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(430, 10, 180, 20);

        jTabbedPane1.addTab("Produto", jPanel1);

        getContentPane().add(jTabbedPane1);
        jTabbedPane1.setBounds(0, 240, 900, 350);

        jButtonSalvar.setText("Adicionar ao Estoque");
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonSalvar);
        jButtonSalvar.setBounds(20, 10, 180, 24);

        jButtonExcluir.setText("Excluir do Estoque");
        jButtonExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonExcluirActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonExcluir);
        jButtonExcluir.setBounds(220, 10, 190, 24);

        jTableProduto.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Id", "Nome", "Preço", "Quantidade", "Quanridade Mínima "
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTableProduto);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(20, 70, 870, 160);

        jLabel1.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel1.setText("Estoque.");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(20, 44, 390, 30);

        setSize(new java.awt.Dimension(910, 630));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
        String tipo = jTextFieldNome.getText();
        String id = jTextFieldId.getText();
        String preco = jTextFieldPreco.getText();
        String quantidade = jTextFieldQuantidade.getText();
        String quantidadeMinima = jTextFieldQuantidadeMin.getText();
         if (jTextFieldNome.getText().equals("") || jTextFieldPreco.getText().equals("") || jTextFieldId.getText().equals("")|| jTextFieldQuantidade.getText().equals("")|| jTextFieldQuantidadeMin.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Preencha todos os campos!");
        } else {
        Produto p = new Produto(tipo, id, preco, quantidade, quantidadeMinima);
        
        System.out.println(p);
        
        dadosProduto.cadastraProduto(p);
        
        JOptionPane.showMessageDialog(this, "Produto Cadastrado com Sucesso");
        carregarListaProdutos();
        
        jTextFieldNome.setText("");
        jTextFieldId.setText("");
        jTextFieldPreco.setText("");
        jTextFieldQuantidade.setText("");
        jTextFieldQuantidadeMin.setText("");
        
         }
        
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jButtonExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExcluirActionPerformed
        if (jTableProduto.getSelectedRowCount()!=0){
        ((DefaultTableModel) jTableProduto.getModel()).removeRow(jTableProduto.getSelectedRow());
        JOptionPane.showMessageDialog(this, "Removido Com Sucesso !");
        }else {
            JOptionPane.showMessageDialog(this, "Selecione o Produto que deseja Remover!");
       }
    }//GEN-LAST:event_jButtonExcluirActionPerformed

    private void jButtonVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVoltarActionPerformed
        BemVIndo telaBemVindo = new BemVIndo();
        telaBemVindo.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButtonVoltarActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       
     if (jTableProduto.getSelectedRowCount()!=0){ 
	DefaultTableModel modelOrigem = (DefaultTableModel) jTableProduto.getModel();
	DefaultTableModel modelDestino = (DefaultTableModel) jTableCompraProdutos.getModel();
	
	Object[] obj = {jTableProduto.getValueAt(jTableProduto.getSelectedRow(), 1),jTableProduto.getValueAt(jTableProduto.getSelectedRow(), 2)};
	
        modelDestino.addRow(obj);       
    } else { 
        JOptionPane.showMessageDialog(this, "Selecione um Produto do Estoque !");
       }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
       if (jTableCompraProdutos.getSelectedRowCount()!=0) {
        ((DefaultTableModel) jTableCompraProdutos.getModel()).removeRow(jTableCompraProdutos.getSelectedRow());
       
      JOptionPane.showMessageDialog(this, "Removido Com Sucesso !");
       }else {
           JOptionPane.showMessageDialog(this, "Selecione o Produto da Lista de Compras Para Remover!");
       }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButtonPagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPagarActionPerformed
        double valorTotal = 0.0;
        String valorInformado = null;
        double troco = 0.0;
        
        for(int linha=0; linha<jTableCompraProdutos.getRowCount();linha++){
        String valorColuna=(String)jTableCompraProdutos.getModel().getValueAt(linha,1); 
        Double valorDouble = Double.parseDouble(valorColuna);       
        
        valorTotal = valorDouble + valorTotal;
                
        }
      String formaDePagamento = null;
      
      try {
      formaDePagamento = JOptionPane.showInputDialog(null, "Insira 1 Cartão ou 2 Dinheiro");
           
      Integer forma = Integer.parseInt(formaDePagamento);   
     
        switch(forma) {
            
            case 1:
                JOptionPane.showMessageDialog(this, "Forma de Pagamento CARTÃO");
                JOptionPane.showMessageDialog(this, "O Valor da Compra é de: R$"+ valorTotal);
                JOptionPane.showMessageDialog(this, "Pagamento Realizado Com Sucesso");
                limpaLista();
                break;
            case 2:
                JOptionPane.showMessageDialog(this, "Forma de Pagamento DINHEIRO");
                JOptionPane.showMessageDialog(this, "O Valor da Compra é de: R$"+ valorTotal);
                valorInformado = JOptionPane.showInputDialog(null, "Insira o Dinheiro");
                double temp = Double.parseDouble(valorInformado);
                if (valorTotal <= temp) {
                    troco = temp - valorTotal;
                     JOptionPane.showMessageDialog(this, "O Valor do Troco é de: R$"+ troco);
                     limpaLista();
                }else {
                    JOptionPane.showMessageDialog(this, "Pagamento Insuficiente");
                }
                
                break;
             
            default:
                JOptionPane.showMessageDialog(this, "Forma de Pagamento Inválida");
                 break;
        }
      
            } catch (NumberFormatException e) {
             JOptionPane.showMessageDialog(null, "Informe apenas números");
        }
       
    }//GEN-LAST:event_jButtonPagarActionPerformed

    
    public static void main(String args[]) {
    
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

    
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaPrincipal().setVisible(true);
            }
        });
    }

    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButtonExcluir;
    private javax.swing.JButton jButtonPagar;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JButton jButtonVoltar;
    private javax.swing.JInternalFrame jInternalFrame1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabelId;
    private javax.swing.JLabel jLabelNome;
    private javax.swing.JLabel jLabelPreco;
    private javax.swing.JLabel jLabelQuantidade;
    private javax.swing.JLabel jLabelQuantidadeMIn;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTableCompraProdutos;
    private javax.swing.JTable jTableProduto;
    private javax.swing.JTextField jTextFieldId;
    private javax.swing.JTextField jTextFieldNome;
    private javax.swing.JTextField jTextFieldPreco;
    private javax.swing.JTextField jTextFieldQuantidade;
    private javax.swing.JTextField jTextFieldQuantidadeMin;
    // End of variables declaration//GEN-END:variables
}
