
package view;

import controller.ValidaEntrada;
import controller.ValidaEntradaLetra;
import controller.dadosCliente;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


import model.Cliente;


public class Pedido extends javax.swing.JFrame {

    dadosCliente dadosClientes = new dadosCliente();

    private Cliente cliente;


    public Pedido() {
        initComponents();
        jTextFieldCelular.setDocument(new ValidaEntrada());
        jTextFieldNome.setDocument(new ValidaEntradaLetra());
        jTextFieldEndereco.setDocument(new ValidaEntradaLetra());
           
    }


    private void carregarListaClientes() {
        ArrayList<Cliente> listaClientes = dadosClientes.getListaCliente();
        DefaultTableModel model = (DefaultTableModel) jTableClientes.getModel();
        model.setRowCount(0);
        for (Cliente c : listaClientes) {
            model.addRow(new String[]{c.getNome(), c.getCelular(), c.getEndereco()});
        }
        jTableClientes.setModel(model);
    }
    
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTableClientes = new javax.swing.JTable();
        jButtonExcluir = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanelUsuario = new javax.swing.JPanel();
        jLabelNome = new javax.swing.JLabel();
        jLabelCelular = new javax.swing.JLabel();
        jLabelEndereco = new javax.swing.JLabel();
        jTextFieldNome = new javax.swing.JTextField();
        jTextFieldCelular = new javax.swing.JTextField();
        jTextFieldEndereco = new javax.swing.JTextField();
        jButtonVoltar = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setSize(new java.awt.Dimension(833, 546));
        getContentPane().setLayout(null);

        jTableClientes.setModel(new javax.swing.table.DefaultTableModel 
            (
                null,
                new String [] {
                    "Nome", "Celular", "Bairro"
                }
            )
            {
                @Override    
                public boolean isCellEditable(int rowIndex, int mColIndex) {
                    return false;
                }
            });
            jScrollPane1.setViewportView(jTableClientes);

            getContentPane().add(jScrollPane1);
            jScrollPane1.setBounds(10, 40, 870, 190);

            jButtonExcluir.setText("Excluir");
            jButtonExcluir.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonExcluirActionPerformed(evt);
                }
            });
            getContentPane().add(jButtonExcluir);
            jButtonExcluir.setBounds(140, 10, 90, 24);

            jPanelUsuario.setLayout(null);

            jLabelNome.setText("Nome :");
            jPanelUsuario.add(jLabelNome);
            jLabelNome.setBounds(30, 10, 150, 30);

            jLabelCelular.setText("Celular :");
            jPanelUsuario.add(jLabelCelular);
            jLabelCelular.setBounds(30, 50, 150, 30);

            jLabelEndereco.setText("Bairro:");
            jPanelUsuario.add(jLabelEndereco);
            jLabelEndereco.setBounds(30, 90, 150, 30);
            jPanelUsuario.add(jTextFieldNome);
            jTextFieldNome.setBounds(80, 10, 300, 30);
            jPanelUsuario.add(jTextFieldCelular);
            jTextFieldCelular.setBounds(90, 50, 310, 30);

            jTextFieldEndereco.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jTextFieldEnderecoActionPerformed(evt);
                }
            });
            jPanelUsuario.add(jTextFieldEndereco);
            jTextFieldEndereco.setBounds(80, 90, 300, 30);

            jButtonVoltar.setText("Voltar");
            jButtonVoltar.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonVoltarActionPerformed(evt);
                }
            });
            jPanelUsuario.add(jButtonVoltar);
            jButtonVoltar.setBounds(540, 90, 130, 40);

            jTabbedPane1.addTab("Usuário", jPanelUsuario);

            getContentPane().add(jTabbedPane1);
            jTabbedPane1.setBounds(0, 240, 880, 310);

            jButton1.setText("Salvar");
            jButton1.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButton1ActionPerformed(evt);
                }
            });
            getContentPane().add(jButton1);
            jButton1.setBounds(10, 10, 110, 24);

            setSize(new java.awt.Dimension(893, 576));
            setLocationRelativeTo(null);
        }// </editor-fold>//GEN-END:initComponents

    private void jButtonExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExcluirActionPerformed
      if (jTableClientes.getSelectedRowCount()!=0){
        ((DefaultTableModel) jTableClientes.getModel()).removeRow(jTableClientes.getSelectedRow());
      JOptionPane.showMessageDialog(this, "Removido Com Sucesso !");
      }else {
          JOptionPane.showMessageDialog(this, "Selecione o Usuário!");
      }
    }//GEN-LAST:event_jButtonExcluirActionPerformed

    private void jTextFieldEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldEnderecoActionPerformed
        
    }//GEN-LAST:event_jTextFieldEnderecoActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       
        String nome = jTextFieldNome.getText();
        String celular = jTextFieldCelular.getText();
        String endereco = jTextFieldEndereco.getText();
        
        if (jTextFieldCelular.getText().equals("") || jTextFieldEndereco.getText().equals("") || jTextFieldNome.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Preencha todos os campos!");
        } else {
        
        Cliente c = new Cliente(nome, endereco, celular);
        
        dadosClientes.cadastraCliente(c);
        
        System.out.println(c);
        
        JOptionPane.showMessageDialog(this, "Contato Cadastrado com Sucesso");
        carregarListaClientes();
        
        jTextFieldNome.setText("");
        jTextFieldCelular.setText("");
        jTextFieldEndereco.setText("");
        } 
    
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButtonVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVoltarActionPerformed
        BemVIndo telaBemVindo = new BemVIndo();
        telaBemVindo.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButtonVoltarActionPerformed

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Pedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Pedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Pedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Pedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>


        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Pedido().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButtonExcluir;
    private javax.swing.JButton jButtonVoltar;
    private javax.swing.JLabel jLabelCelular;
    private javax.swing.JLabel jLabelEndereco;
    private javax.swing.JLabel jLabelNome;
    private javax.swing.JPanel jPanelUsuario;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTableClientes;
    private javax.swing.JTextField jTextFieldCelular;
    private javax.swing.JTextField jTextFieldEndereco;
    private javax.swing.JTextField jTextFieldNome;
    // End of variables declaration//GEN-END:variables
}
